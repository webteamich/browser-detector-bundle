<?php

namespace Truelab\Bundle\BrowserDetectorBundle\Twig;

use Truelab\Bundle\BrowserDetectorBundle\BrowserDetector\BrowserDetector;


class TruelabBrowserDetectorExtension extends \Twig_Extension
{

    protected $browserDetector;

    public function __construct(BrowserDetector $browserDetector)
    {
        $this->browserDetector = $browserDetector;
    }


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('browser_is_compatible', array($this->browserDetector, 'isCompatible'),array('is_safe' => array('all'))),
            new \Twig_SimpleFunction('browser_is_incompatible', array($this->browserDetector, 'isIncompatible'),array('is_safe' => array('all'))),
            new \Twig_SimpleFunction('browser_is_partially_compatible', array($this->browserDetector, 'isPartiallyCompatible'),array('is_safe' => array('all'))),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'truelab_browser_detector_extension';
    }
}