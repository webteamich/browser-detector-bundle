<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        return array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            // register the other bundles your tests depend on

            // and don't forget your own bunde!
            new Truelab\Bundle\BrowserDetectorBundle\TruelabBrowserDetectorBundle(),
        );
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
        //$loader->load(__DIR__.'/config/config.yml');
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return __DIR__.'/cache';
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return __DIR__.'/logs';
    }
}